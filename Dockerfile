
FROM python:3.10-alpine

COPY ./requirements.txt /code/requirements.txt

WORKDIR /code

RUN pip install -r requirements.txt

COPY . /code

ENTRYPOINT [ "python" ]

CMD ["app.py" ]
