from flask import Blueprint, jsonify

blueprints = Blueprint('blueprints', __name__, url_prefix="/blueprints")

@blueprints.route('/items')
def myorders():
    res = {"brand": "Ford","model": "Mustang","year": 1964}
    return jsonify(res)

@blueprints.route('/buy')
def placeorder():
    return "Buy items."

@blueprints.route('/sell')
def cancelorder():
    return "Sell items"