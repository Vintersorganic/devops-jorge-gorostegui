from flask import Flask
# Sólo blueprints
from routes.items import blueprints
# Usando Flask-RESTful
from flask_restful import Api 

from routes.items_resources import Items, BuyItems, SellItems

app = Flask(__name__)

# Registrar blueprint
app.register_blueprint(blueprints)

api = Api(app)

api.add_resource(Items, '/restful/items')
api.add_resource(SellItems, '/restful/buy')
api.add_resource(BuyItems, '/restful/sell')


app.run(debug=True, port=8080, host='0.0.0.0')

 